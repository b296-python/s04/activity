from abc import ABC, abstractmethod
class Animal(ABC):
	@abstractmethod
	def eat(self,food):
		pass
	@abstractmethod
	def make_sound(self):
		pass

class Cat(Animal):
	def __init__(self, name, age, breed):
		super().__init__()
		self._name = name
		self._age = age
		self._breed = breed


	def eat(self, food):
		print(f"Serve me {food}")

	def make_sound(self):
		print("Miaow! Nyaw, Nyaaaaa!")

	def get_name(self):
		print(f"{self._name}")
	def get_age(self):
		print(f"{self._age}")
	def get_breed(self):
		print(f"{self._breed}")

	def set_name(self,name):
		self._name = name
	def set_age(self,age):
		self._age = age
	def set_breed(self,breed):
		self._breed = breed

	def call(self):
		print(f"{self._name}, come on!")


class Dog(Animal):
	def __init__(self, name, age, breed):
		super().__init__()
		self._name = name
		self._age = age
		self._breed = breed

	def eat(self,food):
		print(f"Eaten {food}")

	def make_sound(self):
		print("Bark! woof! Ark!")

	def get_name(self):
		print(f"{self._name}")
	def get_age(self):
		print(f"{self._age}")
	def get_breed(self):
		print(f"{self._breed}")

	def set_name(self,name):
		self._name = name
	def set_age(self,age):
		self._age = age
	def set_breed(self,breed):
		self._breed = breed

	def call(self):
		print(f"Here {self._name}!")


dog1 = Dog("isis", "dalmantia", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1= Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()